using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading.Tasks;
using System.Threading;

namespace NCMemLeak
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		UIWindow window;
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			window = new UIWindow (UIScreen.MainScreen.Bounds);
			window.RootViewController = new UINavigationController(new RootController ());
			window.MakeKeyAndVisible ();
			
			return true;
		}

		class RootController : UIViewController
		{
			public RootController ()
			{
				NavigationItem.RightBarButtonItem = new UIBarButtonItem("Present", UIBarButtonItemStyle.Bordered, (o,e) => {
					PresentViewController(new NavigationController(), true, new NSAction(() => {}));
				});
			}
		}

		class NavigationController : UINavigationController
		{
			public NavigationController ()
				:base(new TestController())
			{
				
			}

			~NavigationController()
			{
				Console.WriteLine("~NavigationController");
			}

			class TestController : UIViewController
			{
				~TestController()
				{
					Console.WriteLine("~TestController");
				}

				public override void ViewDidAppear (bool animated)
				{
					base.ViewDidAppear (animated);
					Task.Factory.StartNew (() => {
						Thread.Sleep(2000);
						NSThread.MainThread.InvokeOnMainThread(new NSAction(() => {
							DismissViewController(true, new NSAction(() => {

							}));
						}));
					});
				}
			}
		}
					
	}
}

